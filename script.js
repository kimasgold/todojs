var list = [{todo: "hello, world!", status: true}];

function generateList() {
    document.querySelector('ul').innerHTML = '';
    for (var i = 0; i < list.length; i++) {
        var str = '<li><label><input type="checkbox" onchange="updateItem(' + i + ')" ' + (list[i].status ? 'checked' : '') + '/><span>' + list[i].todo + '</span></label><button type="button" onclick="remove(' + i + ')">x</button></li>';
        document.querySelector('ul').insertAdjacentHTML('beforeend', str)
    }
    console.log(list);
}

function addNew(e) {
    e.preventDefault();
    var a = document.querySelector('input[type=text]');
    var b = a.value;
    if (b.trim().length > 0) {
        list.push({todo: b, status: false});
        a.value = '';
        generateList();
    }
}

function updateItem(i) {
    list[i].status = !list[i].status;
}

function remove(i) {
    list.splice(i, 1);
    generateList();
}

function removeFinished() {
    for (var i = 0; i < list.length; i++) {
        if(list[i].status) list.splice(i, 1);
    }
    generateList();
}

document.addEventListener('DOMContentLoaded', function () {
    generateList();
    document.querySelector('#note').addEventListener("submit", addNew);
});

